### Dependencies
* docker
* docker-compose

### Setup
* add this to /etc/hosts
```
127.0.0.1       mufasa-amazing.com
127.0.0.1       mufasa-project.com
127.0.0.1       titan-sourcing.com
127.0.0.1       central-api.com
```
* Create folder `docker/volumes`
* Create folder `code/php`
* Go to code/php
* Clone https://bitbucket.org/mcrogado/mufasa_amazing/src/master/
* Clone https://bitbucket.org/mcrogado/mufasa-project/src/master/
* Clone https://bitbucket.org/mcrogado/titan-sourcing/src/master/
* Clone https://bitbucket.org/mcrogado/centralapi/src/master/

### Setup for mufasa_amazing
* Open application/config/development/config.php
* Change the base_url
```php
$config['base_url'] = 'http://mufasa-amazing.com/';
```
* Open application/config/development/database.php
* Set database credentials
```php
$db['default'] = array(
    'dsn'   => '',
    'hostname' => 'database',
    'username' => 'dev',
    'password' => 'dev',
    'database' => 'local_mufasa_db',
    ...
```

### Setup for mufasa project(Main)
* Open application/config/development/config.php
* Change the base_url
```php
$config['base_url'] = 'http://mufasa-project.com/';
```
* Open application/config/development/database.php
* Set database credentials
```php
$db['default'] = array(
    'dsn'   => '',
    'hostname' => 'database',
    'username' => 'dev',
    'password' => 'dev',
    'database' => 'local_mufasa_project_db',
    ...
```

### Setup for Titan Sourcing
* Rename .env.example to .env
* Set database credentials
```php
APP_URL=http://titan-sourcing.com

DB_CONNECTION=mysql
DB_HOST=database
DB_PORT=3306
DB_DATABASE=local_titan_db
DB_USERNAME=dev
DB_PASSWORD=dev
```

### Setup for Central Api
* Rename .env.example to .env
* Set database credentials
```php
APP_URL=http://central-api.com

DB_CONNECTION=mysql
DB_HOST=database
DB_PORT=3306
DB_DATABASE=local_central_api_db
DB_USERNAME=dev
DB_PASSWORD=dev
```

### Running
* Go to project root directory
* docker-compose up

### Run composer install
* docker-compose run mufasa sh -c "cd /app/mufasa_amazing/;composer install"
* docker-compose run mufasa sh -c "cd /app/mufasa-project/;composer install"
* docker-compose run mufasa sh -c "cd /app/titan-sourcing/;composer update"
* docker-compose run mufasa sh -c "cd /app/centralapi/;composer install"

### Generate App Key for Titan Sourcing
```bash
docker-compose run mufasa sh -c "cd /app/titan-sourcing/;php artisan key:generate"
```

### Generate App Key for Central API
* Manually assign APP_KEY
```
    APP_KEY=base64:QUd1g0TnzovPmDWIeFrQIHm8gMd8FG5PhE7V5NGdtgI=
```

### Migration for Mufasa Amazing
```bash
docker-compose run mufasa sh -c "export CI_ENV=development;cd /app/mufasa_amazing/;php index.php cli/migration"
```
### Migration for Mufasa Project(Main)
```bash
docker-compose run mufasa sh -c "export CI_ENV=development;cd /app/mufasa-project/;php index.php cli/migration"
```
### Migration for Titan Sourcing
```bash
docker-compose run mufasa sh -c "cd /app/titan-sourcing/;php artisan migrate"
```

### Migration for Central API
```bash
docker-compose run mufasa sh -c "cd /app/centralapi/;php artisan migrate"
```

### Generate Swagger Docs for Central API
```
php artisan swagger-lume:generate
```

### Frontend Setup
```bash
docker-compose run mufasa sh -c "cd /app/mufasa-project/public/tools/;npm install;gulp build"
docker-compose run mufasa sh -c "cd /app/titan-sourcing;npm install;npm run dev"
```

### To start/stop a service
* docker-compose exec mufasa sh
* supervisorctl status
* supervisorctl start mufasa_amazing
* supervisorctl stop mufasa_amazing

### Opening App
* Open a browser
* Open mufasa-amazing.com
* Open mufasa-project.com
* Open titan-sourcing.com
* Open central-api.com